# Preprocess the data for modeling

drop table eth.prep_combined;
create table eth.prep_combined
select if(merged_using='unknown','not-merged','merged') pr_status
,repo_pr_tenure_mnth
,repo_pr_popularity
,repo_pr_team_size
,round(perc_external_contrib_sub) perc_external_contribs
,round(test_lines_per_kloc*100) test_lines_per_lloc
,round(test_cases_per_kloc*100) test_cases_per_lloc
,round(asserts_per_kloc*100) asserts_per_lloc
,sloc
,prs_country
,prs_pri_same_nationality
,prs_experience
,prev_pullreqs
,round(prs_succ_rate) as prs_succ_rate
,if(test_churn=0,0,1) test_inclusion
,src_churn
,files_changed
,prs_main_team_member
,prs_popularity
,prs_watched_repo
,prs_followed_pri
,num_issue_comments
,if(intra_branch='true',1,0) as intra_branch
from combined
where prs_country in ('australia' , 'belgium',
        'brazil',
        'canada',
        'china',
        'france',
        'germany',
        'india',
        'italy',
        'japan',
        'netherlands',
        'russia',
        'spain',
        'sweden',
        'switzerland',
        'united kingdom',
        'united states'
)
and if(prs_pri_same_nationality=1,if(prs_id=prm_id or prs_id=prc_id,false,true),true);