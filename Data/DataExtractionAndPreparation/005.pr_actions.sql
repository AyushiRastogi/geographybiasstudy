# A comprehensive list of all actions performed by a contributor for a given pull request in a repository at a given timestamp
drop table if exists eth_base.pr_actions;
create table eth_base.pr_actions
select pr.base_repo_id
,pr.id pull_request_id
,actor_id 
,action
,created_at
from github_final.pull_requests pr
,github_final.pull_request_history prh
where pr.id=prh.pull_request_id
union
select pr.base_repo_id
,pr.id pull_request_id
,actor_id 
,action
,ie.created_at
from github_final.pull_requests pr
,github_final.issues i
, github_final.issue_events ie
where pr.id=i.pull_request_id
and ie.issue_id = i.id
union
select pr.base_repo_id
,pr.id pull_request_id
,ic.user_id actor_id 
,'discussed' as action
,ic.created_at
from github_final.pull_requests pr
,github_final.issues i
,github_final.issue_comments ic
where pr.id=i.pull_request_id
and ic.issue_id = i.id
union
select pr.base_repo_id
,pr.id pull_request_id
,prc.user_id actor_id 
,'reviewed' as action
,prc.created_at
from github_final.pull_requests pr
,github_final.pull_request_comments prc
where pr.id=prc.pull_request_id;

alter table eth_base.pr_actions
add index idx(pull_request_id,action,created_at);

################
# Actual actor ids in pull request history
create table eth.pr_actions
select base_repo_id
,pull_request_id
,original actor_id
,action
,created_at
from eth_base.pr_actions pa
left join eth_base.actor_alias aa
on pa.actor_id=aa.actor_id;

alter table eth.pr_actions
add index act(action);