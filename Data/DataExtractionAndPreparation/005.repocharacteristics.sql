# Identify repositories which uses pull based development model
drop table eth_base.repo_id;
create table eth_base.repo_id
select distinct base_repo_id as repo_id
from github_final.pull_requests;

# Create index on repo id
alter table eth_base.repo_id
add index id(repo_id);

#######################
# Identify date of creation of repositories which uses pull based development model
drop table if exists eth_base.repo_created_at;
create table eth_base.repo_created_at
select repo_id 
,created_at
from eth_base.repo_id
left join github_final.projects
on repo_id=id;

#####################
# Language of repository
drop table if exists eth_base.repo_language;
create table eth_base.repo_language
select repo_id
,language
from eth_base.repo_id
left join github_final.projects
on repo_id=id;

###################################
# Textual description of repository
drop table eth_base.repo_description;
create table eth_base.repo_description
select repo_id
,description
from eth_base.repo_id
left join github_final.projects
on repo_id=id;

#####################################
# Repo-PR pairs
drop table eth_base.repo_pr_id;
create table eth_base.repo_pr_id
select distinct base_repo_id
,id as pull_request_id
from github_final.pull_requests;

####################################
# Tenure of the repo at the time the pull request was submitted
drop table if exists eth_base.repo_tenure;
create table eth_base.repo_tenure
select repo_id
,pull_request_id
,timestampdiff(month,rcs.created_at,pfo.created_at) repo_tenure
from eth_base.repo_created_at rcs
left join eth_base.pr_first_opened pfo
on rcs.repo_id=pfo.base_repo_id;

alter table eth_base.repo_tenure
add index pr(pull_request_id);

#######################################
# Watchers count of the repo at the time the pull request was submitted
drop table if exists eth_base.repo_watchers_count;
create table eth_base.repo_watchers_count
select base_repo_id as repo_id
,pull_request_id
,count(*) watchers_count
from github_final.watchers wtch
left join eth_base.pr_first_opened pfo
on wtch.repo_id=pfo.base_repo_id
where wtch.created_at<pfo.created_at
group by base_repo_id,pull_request_id;

#######################################
# Count of previous pull requests received by a repo at the time the pull request was submitted
drop table if exists eth_base.repo_pr_count;
create table eth_base.repo_pr_count
select a.base_repo_id,a.pull_request_id,count(b.pull_request_id) pr_count
from eth_base.pr_first_opened a 
left join eth_base.pr_first_opened b
on a.base_repo_id=b.base_repo_id
where a.created_at>b.created_at
group by a.base_repo_id,a.pull_request_id;
