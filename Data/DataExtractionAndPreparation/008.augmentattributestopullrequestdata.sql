# Add country of residence of submitters, mergers, and closers

## Add columns to accomodate the country information
alter table eth.pull_request_data
add column submitter_country varchar(100) after opened_at;
alter table eth.pull_request_data
add column merger_country varchar(100) after merged_at;
alter table eth.pull_request_data
add column closer_country varchar(100) after closed_at;

## Indexing submitter,merger and closer id 
alter table eth.pull_request_data
add index submitter(submitter);
alter table eth.pull_request_data
add index merger(merger);
alter table eth.pull_request_data
add index closer(closer);

## Add country information of submitter
set sql_safe_updates=0;
update eth.pull_request_data prd
left join eth_base.actor_country ac
on prd.submitter=ac.actor_id
set submitter_country=ac.country;

## Add country information of merger
set sql_safe_updates=0;
update eth.pull_request_data prd
left join eth_base.actor_country ac
on prd.merger=ac.actor_id
set merger_country=ac.country;

## Add country information of closer
set sql_safe_updates=0;
update eth.pull_request_data prd
left join eth_base.actor_country ac
on prd.closer=ac.actor_id
set closer_country=ac.country;

# Select pull requests for which we were able to identify the identify the country of participants
drop table if exists eth.selected_pr_data;
create table eth.selected_pr_data
select *
from eth.pull_request_data
where (status='opened' and submitter is not null and submitter_country!='None')
or (status='merged' and submitter is not null and submitter_country!='None' and merger is not null and merger_country!='None' and ((closer is null) or (closer is not null and closer_country!='None')))
or (status='closed' and submitter is not null and submitter_country!='None' and closer is not null and closer_country!='None');

#####################
# Add column affiliation for pull request participants
alter table eth.selected_pr_data
add column submitter_affiliation varchar(100) after submitter_country;
alter table eth.selected_pr_data
add column merger_affiliation varchar(100) after merger_country;
alter table eth.selected_pr_data
add column closer_affiliation varchar(100) after closer_country;

## Add affiliation information for submitters
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.actor_affiliation aa
on prd.submitter=aa.actor_id
set submitter_affiliation=aa.affiliation;

## Add affiliation information for mergers
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.actor_affiliation aa
on prd.merger=aa.actor_id
set merger_affiliation=aa.affiliation;

## Add affiliation information for closers
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.actor_affiliation aa
on prd.closer=aa.actor_id
set closer_affiliation=aa.affiliation;

##########################################################
# Add tenure of participants 

## Add column to add tenure information of participants
alter table eth.selected_pr_data
add column submitter_tenure int after submitter_affiliation;
alter table eth.selected_pr_data
add column merger_tenure int after merger_affiliation;
alter table eth.selected_pr_data
add column closer_tenure int after closer_affiliation;

## Indexing
alter table eth.selected_pr_data
add index pr_open(pull_request_id,submitter);
alter table eth.selected_pr_data
add index pr_merge(pull_request_id,merger);
alter table eth.selected_pr_data
add index pr_close(pull_request_id,closer);

## Add tenure information for submitters
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.actor_tenure aa
on prd.pull_request_id=aa.pull_request_id
and prd.submitter=aa.original
set submitter_tenure=aa.actor_tenure;

## Add tenure information for mergers
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.actor_tenure aa
on prd.pull_request_id=aa.pull_request_id
and prd.merger=aa.original
set merger_tenure=aa.actor_tenure;

## Add tenure information for closers
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.actor_tenure aa
on prd.pull_request_id=aa.pull_request_id
and prd.closer=aa.original
set closer_tenure=aa.actor_tenure;

#############################
# Repo_language

## Add column to add language information
alter table eth.selected_pr_data
add column repo_language varchar(100) after base_repo_id;

## Indexing
alter table eth.selected_pr_data
add index repo(base_repo_id);

## Add language of a repository
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.repo_language rl
on prd.base_repo_id=rl.repo_id
set repo_language=rl.language;

##################################
# Repo tenure

## Add column to add tenure of repository
alter table eth.selected_pr_data
add column repo_tenure int after repo_language;

## Add tenure of repository
set sql_safe_updates=0;
update eth.selected_pr_data prd
left join eth_base.repo_tenure rt
on prd.pull_request_id=rt.pull_request_id
set prd.repo_tenure=rt.repo_tenure;