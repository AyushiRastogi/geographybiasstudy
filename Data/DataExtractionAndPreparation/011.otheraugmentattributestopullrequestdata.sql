# Same nationality of submitter and integrator

# Add column on same nationality
alter table eth.selected_pr_data
add column same_nationality varchar(5) after status;

## If the nationality of submitter is teh same as the nationality of integrator, we have same nationality
set sql_safe_updates=0;
update eth.selected_pr_data
set same_nationality=if(status='merged' and submitter_country=merger_country,'true',
if(status='closed' and submitter_country=closer_country,'true',
if(status='opened',null,'false')));

##########################################

# Characteritic of pull request - whether it is intra branch or not?

## Add column to add characteristic of pull request
alter table eth.selected_pr_data
add column intra_branch int after pull_request_id;

## Update the characteristic of pull request
set sql_safe_updates=0;
update eth.selected_pr_data spd
left join github_final.pull_requests  pr
on spd.pull_request_id=pr.id
set spd.intra_branch=pr.intra_branch;

##########################################
# Main Team Member

## Add column
alter table eth.selected_pr_data
add column submitter_team_member int after submitter_tenure;

## For a submitter at the time of a pull request, identify the project membership status
set sql_safe_updates=0;
update eth.selected_pr_data spd
left join eth.main_team_member mtm
on spd.base_repo_id=mtm.base_repo_id
and spd.pull_request_id=mtm.pull_request_id
and spd.submitter=mtm.submitter
set submitter_team_member=team_member;

#####################################
# Percentage of external contribution

## Add column
alter table eth.selected_pr_data
add column perc_ext_contrib float unsigned after intra_branch;

## For a project at a time, the percentage of contribution comming from outside
set sql_safe_updates=0;
update eth.selected_pr_data spd
left join eth.perc_ext_contrib pec
on spd.pull_request_id=pec.pull_request_id
set spd.perc_ext_contrib=pec.perc_exter_contrib;

########################
# Prior collaboration ties

# Add column for closed collaboration
alter table eth.selected_pr_data
add column submitter_prior_closed_collaboration int after same_nationality;

## Indexing
alter table eth.prior_closed_collaboration_ties
add index pr(pull_request_id);

## Past closed collaboration ties
update eth.selected_pr_data spd
left join eth.prior_closed_collaboration_ties pcct
on spd.pull_request_id=pcct.pull_request_id
set submitter_prior_closed_collaboration=prior_collaboration_ties;

## Add column for merged collaborations
alter table eth.selected_pr_data
add column submitter_prior_merged_collaboration int after submitter_prior_closed_collaboration;

## Indexing
alter table eth.prior_merged_collaboration_ties
add index pr(pull_request_id);

## Past merged collaboration ties
update eth.selected_pr_data spd
left join eth.prior_merged_collaboration_ties pmct
on spd.pull_request_id=pmct.pull_request_id
set submitter_prior_merged_collaboration=prior_merged_collaboration_ties;

########################################
# Followers count of submitter prior to the given pull request

## Add column
alter table eth.selected_pr_data
add column submitter_followers_count int after submitter_tenure;

## Indexing
alter table eth.submitter_followers_count
add index pr(pull_request_id);

## Followers count before a given pull request
update eth.selected_pr_data spd
left join eth.submitter_followers_count sfc
on spd.pull_request_id=sfc.pull_request_id
set submitter_followers_count=follower_count;

###########################
# Domain of the repository

## Add index
alter table eth.repo_domain
add index repo(repo_id);

## Add column
alter table eth.selected_pr_data
add column repo_domain varchar(100) after repo_language;

## Domain for a repository
update eth.selected_pr_data spd
left join eth.repo_domain rd
on spd.base_repo_id=rd.repo_id
set repo_domain=Domain;
#########################################################
# Watchers count of a repo

## Add column
alter table eth.selected_pr_data
add column repo_watchers_count int after repo_tenure;

# Add watchers' count for a repo
set sql_safe_updates=0;
update eth.selected_pr_data spd
left join eth.repo_watchers_count rwc
on spd.pull_request_id=rwc.pull_request_id
set spd.repo_watchers_count=rwc.watchers_count;

#########################################
# Past pull request status

## Indexing
alter table eth.requester_success_rate
add index pr(pull_request_id);

## Add column
alter table eth.selected_pr_data
add column submitter_past_pr_count int,
add column submitter_past_opened_pr int,
add column submitter_past_merged_pr int,
add column submitter_past_closed_pr int,
add column submitter_past_success_rate float;

## Submitter past experience for a project
set sql_safe_updates=0;
update eth.selected_pr_data spd
left join eth.requester_success_rate rsr
on spd.pull_request_id=rsr.pull_request_id
set submitter_past_pr_count=past_pr_count
,submitter_past_opened_pr=past_opened_pr
,submitter_past_merged_pr=past_merged_pr
,submitter_past_closed_pr=past_closed_pr
,submitter_past_success_rate=success_rate;

####################
# Watching before submitting
# Following before submitting

## Add column
alter table eth.selected_pr_data
add column watching_before_submitting varchar(5)
,add column following_before_submitting varchar(5);

## Watching before submitting
set sql_safe_updates=0;
update eth.selected_pr_data spd
left join eth.watching_before_submitting wbs
on spd.pull_request_id=wbs.pull_request_id
set watching_before_submitting=if(wbs.pull_request_id is null,'false','true');

## Following before submitting
update eth.selected_pr_data spd
left join eth.following_before_submitting fbs
on spd.pull_request_id=fbs.pull_request_id
set following_before_submitting=if(fbs.pull_request_id is null,'false','true');

# Accounts for cases when the pull request is in open state and we do not know who will close the pull request
update eth.selected_pr_data
set following_before_submitting=if(status='opened',null,following_before_submitting);

###########################################
# Team size

## Add column
alter table eth.selected_pr_data
add column team_size int after intra_branch;

## Set size of the team at the time of pull request
set sql_safe_updates=0;
update eth.selected_pr_data spd
left join eth.team_size ts
on spd.pull_request_id=ts.source_pr
set spd.team_size=ts.team_size;