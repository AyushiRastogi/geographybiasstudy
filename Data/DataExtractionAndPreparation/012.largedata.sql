drop table eth.pr_large_data;
create table eth.pr_large_data
select base_repo_id as repo_id
,repo_language
,repo_domain
,if(repo_tenure<0,null,repo_tenure) as repo_pr_tenure_mnth # account for data error
,repo_watchers_count as repo_pr_popularity
,team_size as repo_pr_team_size
,perc_ext_contrib*100 as repo_pr_perc_ext_contrib # converts fraction to percentage
,pull_request_id as pr_id
,intra_branch as pr_intra_branch
,unix_timestamp(opened_at) as pr_opened_at 
,if(merged_at is not null,unix_timestamp(merged_at),0) as pr_merged_at
,if(closed_at is not null,unix_timestamp(closed_at),0) as pr_closed_at
,status as pr_status
,submitter as prs_id
,submitter_team_member as prs_main_team_member
,submitter_country as prs_country
,submitter_affiliation as prs_affiliation
,if(submitter_tenure<0,null,submitter_tenure) as prs_tenure_mnth # accounts for data error
,submitter_followers_count as prs_popularity
,submitter_past_pr_count as prs_experience
,submitter_past_opened_pr as prs_open_pr
,submitter_past_merged_pr as prs_merged_pr
,submitter_past_closed_pr as prs_closed_pr
,submitter_past_success_rate*100 as prs_succ_rate # converts fraction to percentage
,submitter_prior_merged_collaboration as prs_prm_past_collab
,submitter_prior_closed_collaboration as prs_prc_past_collab
,if(same_nationality is not null,if(same_nationality='true',1,0),null) as prs_pri_same_nationality # true: 1, false: 0
,if(watching_before_submitting='true',1,0) as prs_watched_repo
,if(following_before_submitting is not null,if(following_before_submitting='true',1,0),null) as prs_followed_pri # true: 1, false: 0
,merger as prm_id
,merger_country as prm_country
,merger_affiliation as prm_affiliation
,if(merger_tenure<0,null,merger_tenure) as prm_tenure_mnth # accounts for data error
,closer as prc_id
,closer_country as prc_country
,closer_affiliation as prc_affiliation
,if(closer_tenure<0,null,closer_tenure) as prc_tenure_mnth # accounts for data error
from eth.selected_pr_data;

alter table eth.pr_large_data
add index pr(pr_id);