# A pull request can undergo various stages in its lifecycle
# It may go in loop. For instance, it may get reopened after being closed
# Here, we focus on the first cycle of the pull request, i.e. opened-merged-closed
# We eliminate pull request which do not follow the above-stated lifecycle, like closed-opened. etc.

# Opened states of pull requests
drop table if exists eth.pr_opened;
create table eth.pr_opened
select base_repo_id
,pull_request_id
,actor_id
,created_at
from eth.pr_actions
where action='opened';

# Merged states of pull requests
drop table if exists eth.pr_merged;
create table eth.pr_merged
select base_repo_id
,pull_request_id
,actor_id
,created_at
from eth.pr_actions
where action='merged';

# Closed states of pull requests
drop table if exists eth.pr_closed;
create table eth.pr_closed
select base_repo_id
,pull_request_id
,actor_id
,created_at
from eth.pr_actions
where action='closed';

# Idexing the three states of a pull request with timestamp
alter table eth.pr_opened
add index idx(pull_request_id,created_at);

alter table eth.pr_merged
add index idx(pull_request_id,created_at);

alter table eth.pr_closed
add index idx(pull_request_id,created_at);

#####################################

## Identify the timestamp when the pull request was first opened
drop table if exists eth.pr_first_opened;
create table eth.pr_first_opened
select base_repo_id
,pull_request_id
,actor_id
,created_at
from eth.pr_opened mt
where created_at=
(select min(created_at) from eth.pr_opened st where mt.pull_request_id=st.pull_request_id);

## Identify the timestamp when the pull request was first merged, if at all
drop table if exists eth_base.pr_first_merged;
create table eth.pr_first_merged
select base_repo_id
,pull_request_id
,actor_id
,created_at
from eth.pr_merged mt
where created_at=
(select min(created_at) from eth.pr_merged st where mt.pull_request_id=st.pull_request_id);

## Identify the timestamp when the pull request was first closed, if at all
drop table if exists eth.pr_first_closed;
create table eth.pr_first_closed
select base_repo_id
,pull_request_id
,actor_id
,created_at
from eth.pr_closed mt
where created_at=
(select min(created_at) from eth.pr_closed st where mt.pull_request_id=st.pull_request_id);

#############################
# Indexing the three tables on pull request id to faciliate merging the pull request lifecycle
alter table eth.pr_first_opened
add index pr(pull_request_id);
alter table eth.pr_first_merged
add index pr(pull_request_id);
alter table eth.pr_first_closed
add index pr(pull_request_id);

########################################
# Create the lifecycle of the pull request by combining the first time a given pull request was opened, merged, and closed
# The data generated by this table may voilate integrity constraints as
# reportedly some pull requests may seems to be closed before they were opened
# This may happen due to missing data in the GHTorrent dataset or some missing steps in the pull request lifecycle
drop table if exists eth.pr_lifecycle;
create table eth.pr_lifecycle
select opn.base_repo_id
,opn.pull_request_id
,opn.actor_id as submitter
,opn.created_at opened_at
,mrg.actor_id as merger
,mrg.created_at merged_at
,clsd.actor_id as closer
,clsd.created_at closed_at
from eth.pr_first_opened opn
left join eth.pr_first_merged mrg
on opn.pull_request_id=mrg.pull_request_id
left join eth.pr_first_closed clsd
on opn.pull_request_id=clsd.pull_request_id;

## Enforce the following asserts on the data and classify pull requests as opened, merged and closed
# Checks asserts
# Opened_at<=Merged_at
# Opened_at<=Closed_at
# Merged_at<=Closed_at
# If the pull request is neither merged nor closed, it is in opened state
# If the pull request is closed but not merged, it is closed
# Else if the pull request is merged and may/may not be closed, it is merged
# The actors correspoding to open, merge, and closed action are termed as submitter, merger, and closer respectively
drop table if exists eth.pull_request_data;
create table eth.pull_request_data
select *
,if(opened_at<=merged_at,'merged', 
if(merged_at is null and opened_at<=closed_at,'closed', 
if(merged_at is null and closed_at is null,'opened','other'))) status
from eth.pr_lifecycle;