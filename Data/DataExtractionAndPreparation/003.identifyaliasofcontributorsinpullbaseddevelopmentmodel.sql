# Not all contributors on GitHub use pull based developement model
# We use the following code to identify all contributors who participated in pull requests in any form
create table eth_base.pr_actors
select distinct actor_id
from github_final.pull_request_history
where actor_id is not null;

# Indexing contributors on actor_id
alter table eth_base.pr_actors
add index idx(actor_id);

# Generate a csv file where we use the login, name, location and email of a contributor to identify the presence of duplicate code
select id
,login
,if(name is null or name='','None',name) name
,if(location is null,'None',location) location
,if(email is null,'None',email) email
into outfile "github_final.csv"
character set utf8
fields terminated by ';'
OPTIONALLY ENCLOSED BY '"'
from eth_base.pr_actors act
left join github_final.users usr
on act.actor_id=usr.id
where type='USR'; 

# Import the mapping of fake and original id into the following table as identified by the MergeAlias code written by Bogdan Vasilescu 
# https://github.com/bvasiles/ght_unmasking_aliases

# Schema for import
create table eth_base.MergeAlias
(fake int,
original int
);

# Importing the data
LOAD DATA LOCAL INFILE "C:/Python27/Lib/site-packages/ght_unmasking_aliases-master/idm/idm_map_final.csv"
INTO TABLE eth_base.MergeAlias
COLUMNS TERMINATED BY ';'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

# Indexing the fake id to allow mapping to the original id
alter table eth_base.mergealias
add index fake(fake);