# Total pull request counts in the data
select count(pull_req_id) pull_req_count
from eth_base.pull_request_data; # 370,411

# Total repos in the data 
select count(distinct project_id) repo_count
from eth_base.pull_request_data; # 1069

select count(distinct requester_id) requester_count
from eth_base.pull_request_data; #45,243


