# Set the path to the directory where the pull request data for multiple projects are stored
for file in *.csv
do
    mysql -e "LOAD DATA LOCAL INFILE '"$file"' INTO TABLE pull_request_data FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES" -u <user name> -P <port number> -h <host> -p <password> --local-infile eth_base
echo "Done: '"$file"' at $(date)"
done