# A schema to import the pull request data published by Gousious et al. 

drop table if exists eth_base.pull_request_data;
create table eth_base.pull_request_data
(pull_req_id int
,project_name varchar(100)
,lang varchar(20)
,github_id int
,created_at int
,merged_at int
,closed_at int
,lifetime_minutes int
,mergetime_minutes int 
,merged_using varchar(20)
,conflict varchar(5)
,forward_links varchar(5)
,team_size int
,num_commits int
,num_commit_comments int
,num_issue_comments int
,num_comments int
,num_participants int
,files_added int
,files_deleted int
,files_modified int
,files_changed int
,src_files int
,doc_files int
,other_files int
,perc_external_contribs int
,sloc int
,src_churn int
,test_churn int
,commits_on_files_touched int
,test_lines_per_kloc decimal(15,15)
,test_cases_per_kloc decimal(15,15)
,asserts_per_kloc decimal(15,15)
,watchers int
,requester varchar(20)
,prev_pullreqs int
,requester_succ_rate decimal(15,15)
,followers int
,intra_branch varchar(5)
,main_team_member varchar(5)
)

# The files are imported from the following url https://github.com/AyushiRastogi/pullreqs/tree/master/data
