# Additional information required for analysis

# Add project id
## Add project_id column to the table
alter table eth_base.pull_request_data
add project_id int after pull_req_id;

## Identify full name of the repository
drop table if exists github_final.repo_full_name;
create table github_final.repo_full_name
select id repo_id
,substring(url,30) repo_full_name
from github_final.projects;

## Add indexing on full name of the repository
alter table github_final.repo_full_name
add index full_name(repo_full_name(10));

## Matching project name to project id
select distinct project_name
, repo_id as project_id
from eth_base.pull_request_data prd
left join github_final.repo_full_name rfn
on prd.project_name=rfn.repo_full_name; # project name is a subpart of the url specifying directory structure;

## Add project_id to the data
set sql_safe_updates=0;
update eth_base.pull_request_data prd
join github_final.repo_full_name rfn
on prd.project_name=rfn.repo_full_name
set project_id=repo_id;

#####################################

# Add requester id
## Add requester id column 
alter table eth_base.pull_request_data
add requester_id int after watchers;

## Indexing name in users table
alter table github_final.users
add index name(name);

## Update requester id for a given login
set sql_safe_updates=0;
update eth_base.pull_request_data prd
left join github_final.users usr
on prd.requester=usr.login
set requester_id=usr.id;

## Update requester id to original where fake
update eth_base.pull_request_data prd
left join eth_base.mergeAlias ma
on prd.requester_id=ma.fake
set prd.requester_id=ma.original
where prd.requester_id=ma.fake;

################################

# Requester country
## Add requester country column
alter table eth_base.pull_request_data
add requester_country varchar(100) after requester;

## Update country information for a given requester 
set sql_safe_updates=0;
update eth_base.pull_request_data prd
left join eth_base.actor_country ac
on prd.requester_id=ac.actor_id
set requester_country=ac.country;

####################################

# Requester affiliation
## Add requester affiliation column
alter table eth_base.pull_request_data
add requester_affiliation varchar(100) after requester_country;

## Update affiliation information for a given requester
set sql_safe_updates=0;
update eth_base.pull_request_data prd
left join eth_base.actor_affiliation aa
on prd.requester_id=aa.actor_id
set requester_affiliation=aa.affiliation;

#####################################
# Requester tenure
## Add requester tenure column
alter table eth_base.pull_request_data
add requester_tenure int after requester_affiliation;

## Update tenure information for a given requester
set sql_safe_updates=0;
update eth_base.pull_request_data prd
left join eth_base.actor_tenure aten
on prd.pull_req_id=aten.pull_request_id
and prd.requester_id=aten.original
set requester_tenure=aten.actor_tenure;

#####################################
# Project tenure
## Add project tenure column
alter table eth_base.pull_request_data
add project_tenure int after lang;

## Update tenure information for a project
set sql_safe_updates=0;
update eth_base.pull_request_data prd
left join eth_base.repo_tenure rt
on prd.pull_req_id=rt.pull_request_id
set project_tenure=rt.repo_tenure;

#####################################
# Project domain
## Add domain column
alter table eth_base.pull_request_data
add column domain varchar(100) after lang;

## Update domain information for a project
update eth_base.pull_request_data prd
left join eth.repo_domain rd
on prd.project_id=rd.repo_id
set prd.domain=rd.Domain;

