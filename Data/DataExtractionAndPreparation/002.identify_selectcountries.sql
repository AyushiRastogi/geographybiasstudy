# Create an index on column location in users table
alter table github_final.users
add index loc(location);

# Step 1: Identify unique locations of all users
drop table if exists eth_base.location;
create table eth_base.location
select distinct location
from github_final.users
where type='usr'
and location is not null;

# Save results as csv
select location
from eth_base.location;

# Identify country based on the location specified by the contributors

# Create a table to import the country corresponding to the location specified by the contributors
drop table if exists eth_base.countryNameManager;
create table eth_base.countryNameManager
(location varchar(255),
country varchar(255)
);

# Import the data 
LOAD DATA LOCAL INFILE "E:/Ayushi Rastogi/Geography/CSV/github_final_location_country.csv"
INTO TABLE eth_base.countryNameManager
COLUMNS TERMINATED BY ';'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

# Add index to the mapping column, i.e. location
alter table eth_base.countryNameManager
add index idx(location);

# Prepare the data to identify country of residence of contributors based on heuristics 
drop table if exists eth_base.locationBasedCountry;
create table eth_base.locationBasedCountry
select id user_id
,company
,substring_index(email,'@',-1) email_domain
,if(country is null,"None",country) country
from github_final.users usr
left join eth_base.countryNameManager cnm
on usr.location=cnm.location;

# We needed to do this as we worked on Windows and Mac machine 
set sql_safe_updates=0;
update eth_base.locationbasedcountry
set country=replace(country,'\r',''); # Eliminate extra \r appended to each country

# Add index to the table created to identify country of residence of contributors based on heuristics
alter table eth_base.locationBasedCountry
add index idx(user_id,company,email_domain,country(10));

# Map company to country
drop table if exists eth_base.companyBasedCountryData;
create table eth_base.companyBasedCountryData
select company
,country
,count(*) count
from eth_base.locationBasedCountry
where company is not null
and country !='None'
group by company,country;

# Map email domain to country
drop table if exists eth_base.domainBasedCountryData;
create table eth_base.domainBasedCountryData
select email_domain
,country
,count(*) count
from eth_base.locationBasedCountry
where email_domain is not null
and country !='None'
group by email_domain,country;

# Define rules to identify country for contributors with specific characteristics
## If the company is localized to 1 country and has atleast 10 contributors, we map the company to the country
## For instance, Queen's University maps to Canada
drop table if exists eth_base.rulesOnCompany;
create table eth_base.rulesOnCompany
select company
,count(country) country_count
,sum(count) dev_count
,country
from eth_base.companyBasedCountryData
group by company
having country_count=1
and dev_count>=10;

# Indexing the mapping parameter - company
alter table eth_base.rulesOnCompany
add index company(company);

## If the domain is localized to 1 country and has atleast 10 contributors, we map the domain to the country
## For instance, virginia.edu maps to United States 
drop table if exists eth_base.rulesOnDomain;
create table eth_base.rulesOnDomain
select email_domain
,count(country) country_count
,sum(count) dev_count
,country
from eth_base.domainBasedCountryData
group by email_domain
having country_count=1
and dev_count>=10;

# Indexing on the mapping parameter - domain
alter table eth_base.rulesOnDomain
add index domain(email_domain);

# Identify the countries of contributors, for whom location is not mentioned otherwise, based on their company and email domain
drop table if exists eth_base.countryOnHeuristics;
create table eth_base.countryOnHeuristics
select loc.*
,roc.country companyCountry
,rod.country domainCountry
from eth_base.locationBasedCountry loc
left join eth_base.rulesOnCompany roc
on loc.company=roc.company
left join eth_base.rulesOnDomain rod
on loc.email_domain=rod.email_domain
where loc.country='None';

# Indexing on the mapping parameter - userid here
alter table eth_base.countryOnHeuristics
add index idx(user_id);

# Countries identified through location and heuristics
## First we try to infer the country based on location mentioned in the textual description
## Then, we try to infer the country based on the email domain
## Finally, we try to infer the country based on the company
### The order is defined based on the perceived relevance of the ability of the field to identify the country
create table eth_base.country
select lbc.user_id
,if(lbc.country='None',
if(domainCountry is null,if(companyCountry is null,'None',companyCountry),domainCountry),lbc.country) country
from eth_base.locationBasedCountry lbc
left join eth_base.countryOnHeuristics coh
on lbc.user_id=coh.user_id;

# Indexing on the mapping paramter - userid
alter table eth_base.country
add index idx(user_id);

# Import the mapping of countries to continent as in 2015-06-25.dump.countrylist.net
# The imported table is titled countrys_int
# Add index on the imported table
alter table eth_base.countrys_int
add index idx(name);

# Map countries to continents where country is not 'None'
drop table eth_base.user_nat_cont;
create table eth_base.user_nat_cont
select user_id,country,continent
from eth_base.country con
left join eth_base.countrys_int ci
on con.country=ci.name
where country!='None';

# The list misses on the following few countries, which are manually specified 
set SQL_SAFE_UPDATES=0;
update eth_base.user_nat_cont
set continent='Europe'
where country='russia';

update eth_base.user_nat_cont
set continent='Asia'
where country='south korea';

update eth_base.user_nat_cont
set continent='Asia'
where country='vietnam';

update eth_base.user_nat_cont
set continent='Asia'
where country='iran';

update eth_base.user_nat_cont
set continent='Europe'
where country='macedonia';

update eth_base.user_nat_cont
set continent='Asia'
where country='macau';

update eth_base.user_nat_cont
set continent='Asia'
where country='syria';

update eth_base.user_nat_cont
set continent='Asia'
where country='palestinian territories';

update eth_base.user_nat_cont
set continent='Europe'
where country='kosovo';

update eth_base.user_nat_cont
set continent='Africa'
where country='tanzania';

update eth_base.user_nat_cont
set continent='Europe'
where country='reunion';

update eth_base.user_nat_cont
set continent='Africa'
where country='cote d\'ivoire';

update eth_base.user_nat_cont
set continent='Asia'
where country='brunei';

update eth_base.user_nat_cont
set continent='North America'
where country='virgin islands';

update eth_base.user_nat_cont
set continent='North America'
where country='sint maarten';

update eth_base.user_nat_cont
set continent='Africa'
where country='libya';

update eth_base.user_nat_cont
set continent='Asia'
where country='laos';

update eth_base.user_nat_cont
set continent='Europe'
where country='vatican city';

update eth_base.user_nat_cont
set continent='Africa'
where country='south sudan';

update eth_base.user_nat_cont
set continent='North America'
where country='curacao';

update eth_base.user_nat_cont
set continent='Asia'
where country='north korea';

update eth_base.user_nat_cont
set continent='Europe'
where country='cocos islands';

#############################
# Indexing on user_id
alter table eth_base.user_nat_cont
add index idx(user_id);

# We were able to identify the country of 502,706 contributors out of a total of 4,112,994 contributors on GitHub 
# (based on GHTorrent data available in Auguest 2015)
select count(*) total
from eth_base.user_nat_cont; # 502,706

select count(*)
from github_final.users
where type='usr'; # 4,112,994

# Assuming that the contributors for whom the country information is missing is distributed uniformly
# We select countries which represent at least 1% of the total population of contributors for whom we can identify the country
# This also ensures that we select at least 75% of the total population of contributors for whom we can identify the country of residence
drop table if exists eth_base.selectedCountriesOnStats;
set @cum_sum:=0;
set @cum_perc:=0;
create table eth_base.selectedCountriesOnStats
select continent,country
,count,@cum_sum:=@cum_sum+count as cum_sum
,percentage, @cum_perc:=@cum_perc+percentage as cum_percentage
from(
select continent
, country
,count(*) count
,((count(*)*100)/(select count(*) total from eth_base.user_nat_cont)) percentage
from eth_base.user_nat_cont
group by continent,country
order by percentage desc) temp
where percentage>=1;

# Selected countries are 
select *
from eth_base.selectedCountriesOnStats;