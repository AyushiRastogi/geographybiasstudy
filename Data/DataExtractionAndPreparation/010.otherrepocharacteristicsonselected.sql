# Watchers count of a repository at the time a given pull request is issued

## Indexing
alter table github_final.watchers
add index wtch(repo_id,created_at);
alter table eth.selected_pr_data
add index wtch(base_repo_id,opened_at);

## Watchers of a repository before a given pull request is issued
drop table if exists eth.repo_watchers;
create table eth.repo_watchers
select pull_request_id
,base_repo_id
,user_id
from eth.selected_pr_data spd
left join github_final.watchers wtch
on spd.base_repo_id=wtch.repo_id
where opened_at>created_at;

## Watchers count of a repository
drop table if exists eth.repo_watchers_count;
create table eth.repo_watchers_count 
select pull_request_id
,base_repo_id
,count(*) watchers_count
from eth.selected_pr_data spd
left join github_final.watchers wtch
on spd.base_repo_id=wtch.repo_id
where opened_at>created_at
group by spd.base_repo_id,opened_at;

# Indexing
alter table eth.repo_watchers_count
add index pr(pull_request_id);

###############################################
# Percentage of external contribution in the project at the time the pull request is issued

## Indexing the base table 
alter table eth.main_team_member
add index pr(pull_request_id);

# Identify the time at which a contributor is a member of the repo
drop table if exists eth.pre_perc_ext_contrib;
create table eth.pre_perc_ext_contrib
select mtm.*
,opened_at
from eth.main_team_member mtm
left join eth.pull_request_data prd
on mtm.pull_request_id=prd.pull_request_id;

## Indexing
alter table eth.pre_perc_ext_contrib
add index repo_open(base_repo_id,opened_at,pull_request_id);

## For a given repository, how much contribution came from contributors other than main team member
drop table if exists eth.perc_ext_contrib;
create table eth.perc_ext_contrib
select a.base_repo_id
,a.pull_request_id,
1-(sum(b.team_member)/count(*)) perc_exter_contrib
from eth.pre_perc_ext_contrib a
left join eth.pre_perc_ext_contrib b
on a.base_repo_id=b.base_repo_id
and a.opened_at>b.opened_at
group by a.pull_request_id;

###########################################
# Team size at the time of pull request

## order pull requests on the date of creation
drop table eth.pr_order;
set @order:=0;
create table eth.pr_order
select @order:=@order+1 as SNo
,base_repo_id
,pull_request_id
,opened_at
from eth.selected_pr_data
order by base_repo_id,opened_at;

## Indexing
alter table eth.pr_order
add index repo_open(SNo,base_repo_id);

## Pull requests opened prior to a specific pull request
drop table eth.pr_source_dest;
create table eth.pr_source_dest
select a.pull_request_id source_pr
,b.pull_request_id des_pr
from eth.pr_order a
,eth.pr_order b
where a.base_repo_id=b.base_repo_id
and a.SNo>b.SNo;

## Indexing
alter table eth.pr_source_dest
add index src_dest(source_pr,des_pr);

## All actors for a pull request
drop table if exists eth.pr_actors;
create table eth.pr_actors
select distinct base_repo_id,pull_request_id,actor_id
from eth.pr_actions
where actor_id is not null;

## Indexing
alter table eth.pr_actors
add index pr(pull_request_id);

## All contributors to a project before a specific pull request was opened
drop table eth.team_size;
create table eth.team_size
select source_pr
,count(distinct actor_id) team_size
from eth.pr_source_dest
left join eth.pr_actors
on des_pr=pull_request_id
group by source_pr;

## Indexing
alter table eth.team_size
add index pr(source_pr);

#############################################
# Domain of a repository based on keywords identified using PCA on a subset of data
drop table if exists eth.repo_domain;
create table eth.repo_domain
select repo_id
,if(lower(description) like '% application%' or lower(description) like '% app%' or lower(description) like '% apps %' or lower(description) like '% tool%' or lower(description) like '% add-ons%' or lower(description) like '% generator%' or lower(description) like '%service%' or lower(description) like '%development%','Application' 
,if(lower(description) like '%database%' or lower(description) like '%mvc%' or lower(description) like '%server%' or lower(description) like '%client%' or lower(description) like '%network%','Database' 
,if(lower(description) like '%compiler%' or lower(description) like '%parser%' or lower(description) like '%interpreter%' or lower(description) like '%bootstrap%' or lower(description) like '%binding%' or lower(description) like '%build%','Compiler' 
,if(lower(description) like '%platform%' or lower(description) like '%client%' or lower(description) like '%server%' or lower(description) like '% port%','MiddleWare' 
,if(lower(description) like '%library%' or lower(description) like '%libraries%' or lower(description) like '% api%' or lower(description) like '%wrapper%' or lower(description) like '%package%' or lower(description) like '%driver%' or lower(description) like '%wrapper%' or lower(description) like '%bindings%' or lower(description) like '%lightweight%','library' 
,if(lower(description) like '%framework%' or lower(description) like'%environment%' or lower(description) like '% sdk%' or lower(description) like '%transforming%' or lower(description) like '%plugin%' or lower(description) like '%interface%' or lower(description) like '%template%' or lower(description) like '%model%','Framework','Others')))))) Domain 
from eth_base.repo_description;



