use eth;

# Select pull requests where integrator is not the same as the submitter
drop table if exists large_survey_data;
create table large_survey_data
select pr_id
,prs_id
,prm_id pri_id
,prs_country
,prm_country pri_country
,pr_status
,prs_main_team_member
from pr_large_data
where prm_id!=prs_id
union
select pr_id
,prs_id
,prc_id pri_id
,prs_country
,prc_country pri_country
,pr_status
,prs_main_team_member
from pr_large_data
where  prc_id!=prs_id;

# Selecting pull request submitters who have submitted 
# at least 10 pull request to integrators from two or more countries
drop table if exists submitters;
create table submitters
select prs_id
,prs_country
,count(*) pr_count
,sum(if(pr_status='merged',1,0)) merged_count
,sum(if(pr_status='closed',1,0)) closed_count
,count(distinct pri_country) country_count
from large_survey_data
group by prs_id
having country_count>=2
and pr_count>=10;

# Selecting pull request integrators who have reviewed
# at least 10 pull requests from submitters from two or more countries
drop table if exists integrators;
create table integrators
select pri_id
,pri_country
,count(*) pr_count
,sum(if(pr_status='merged',1,0)) merged_count
,sum(if(pr_status='closed',1,0)) closed_count
,count(distinct prs_country) country_count
from large_survey_data
group by pri_id
having country_count>=2
and pr_count>=10;

######################################
# Identify contributors who have played the role of integrator only in the selected dataset
drop table integrators_only;
create table integrators_only
select *
from integrators
where pri_id not in (select prs_id from submitters);

# Identify contributors who have played the role of submitters only in the selected dataset
drop table submitters_only;
create table submitters_only
select *
from submitters
where prs_id not in (select pri_id from integrators);

# For contributors, who played the role of both submitter and integrator 

## Submitters are the ones, who played the role of submitter >= integrtor
drop table submitters_role;
create table submitters_role
select sub.*
from submitters sub
,integrators intgr
where prs_id=pri_id
and sub.pr_count>=intgr.pr_count;

## Integrators are the ones, who played the role of integrator > submitter
drop table integrators_role;
create table integrators_role
select intgr.*
from submitters sub
,integrators intgr
where prs_id=pri_id
and sub.pr_count<intgr.pr_count;

# List of selected submitters = submitters only + submitters role
drop table if exists selected_submitters;
create table selected_submitters
select *
from submitters_only
union
select *
from submitters_role;

# List of selectd integrators = integrators only + integrators role
create table selected_integrators
select *
from integrators_only
union
select *
from integrators_role;