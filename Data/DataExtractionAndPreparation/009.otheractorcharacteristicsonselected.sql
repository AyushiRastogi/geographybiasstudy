# Other charcteristics of contributors for selected pull request

# Followers count of pull request submitters at the time of pull request opened

## Identify followers of a submitter before a given pull request is opened
drop table eth.submitter_followers;
create table eth.submitter_followers
select submitter
,pull_request_id
,opened_at
,follower_id
from eth.selected_pr_data spd
left join github_final.followers flwr
on spd.submitter=flwr.user_id
where spd.opened_at>flwr.created_at;

# Indexing
alter table eth.selected_pr_data
add index sub_open(submitter,opened_at);

alter table github_final.followers
add index usr_crtd(user_id,created_at);

# Followers count of a submitter at the time the pull request is issued
drop table if exists eth.submitter_followers_count;
create table eth.submitter_followers_count
select submitter
,pull_request_id
,opened_at
,count(follower_id) follower_count
from eth.selected_pr_data spd
left join github_final.followers flwr
on spd.submitter=flwr.user_id
where spd.opened_at>flwr.created_at
group by spd.submitter,spd.opened_at;

##################################################
# Past collaboration ties

## Indexing
alter table eth.pull_request_data
add index prior_collab(submitter,merger,opened_at,pull_request_id);

## Before the pull request in the study is opened, how many times have submitters and integrators worked together which were merged
drop table eth.prior_merged_collaboration_ties;
create table eth.prior_merged_collaboration_ties
select a.pull_request_id
,a.submitter
,a.merger
,a.opened_at
,count(*) prior_merged_collaboration_ties
from eth.pull_request_data a
,eth.pull_request_data b
where a.submitter=b.submitter
and a.merger=b.merger
and a.opened_at>b.opened_at
and a.merger is not null
group by a.submitter,a.merger,opened_at;

#########

## Indexing
alter table eth.pull_request_data
add index prior_collab_closer(submitter,closer,opened_at);

## Before the pull request in the study is opened, how many times have submitters and integrators worked together which were closed
create table eth.prior_closed_collaboration_ties
select a.pull_request_id
,a.submitter
,a.closer
,a.opened_at
,count(*) prior_collaboration_ties
from eth.pull_request_data a
,eth.pull_request_data b
where a.submitter=b.submitter
and a.closer=b.closer
and a.opened_at>b.opened_at
and a.closer is not null
group by a.submitter,a.closer,a.opened_at;

###################################################
# Previous success rate of the pull request submitter

## Indexing
alter table eth.pull_request_data
add index pr_sub_open(pull_request_id,submitter,opened_at); 

## Out of all the pull request opened, how many where merged or closed?
## Success Rate = Pull request merged/ Total pull request submitted
drop table eth.requester_success_rate;
create table eth.requester_success_rate
select a.submitter
,a.pull_request_id
,count(*) past_pr_count
,sum(if(b.status='opened',1,0)) past_opened_pr
,sum(if(b.status='merged',1,0)) past_merged_pr
,sum(if(b.status='closed',1,0)) past_closed_pr
,sum(if(b.status='merged',1,0))/count(*) success_rate
from eth.pull_request_data a
,eth.pull_request_data b
where a.submitter=b.submitter
and a.opened_at>b.opened_at
group by a.submitter,a.opened_at;

########################################################
# Main Team Member
# Is contributor a member of the team at the time the pull request is submitted?

## Indexing
alter table eth.pull_request_data
add index (base_repo_id,pull_request_id,opened_at);

drop table if exists eth.main_team_member;
create table eth.main_team_member
select a.base_repo_id
,a.pull_request_id
,a.submitter
,if(a.submitter=b.merger or a.submitter=b.closer,1,0) team_member
from eth.pull_request_data a
,eth.pull_request_data b
where a.base_repo_id=b.base_repo_id
and a.opened_at>b.opened_at
group by a.base_repo_id,a.pull_request_id;
######################################################
# Did the submitter watched the repo before submitting the pull request?
drop table if exists eth.watching_before_submitting;
create table eth.watching_before_submitting
select pull_request_id
from eth.selected_pr_data spd
left join github_final.watchers wtch
on spd.base_repo_id=wtch.repo_id
and spd.opened_at>wtch.created_at
where submitter=user_id;

## Indexing
alter table eth.watching_before_submitting
add index pr(pull_request_id);
####################################################
# Did the submitter followed the integrator before submiting the pull request>
drop table if exists eth.following_before_submitting;
create table eth.following_before_submitting
select pull_request_id
from eth.selected_pr_data spd
left join github_final.followers flwr
on (spd.merger=flwr.user_id or spd.closer=flwr.user_id)
and spd.opened_at>flwr.created_at
where submitter=follower_id;

## Indexing
alter table eth.following_before_submitting
add index pr(pull_request_id);

