# Identify all actors involved in pull based software development model. This includes fake ids.
drop table eth_base.pre_actor_id;
create table eth_base.pre_actor_id
select distinct actor_id
from eth_base.pr_actions
where actor_id is not null;

# Indexing on actor id
alter table eth_base.pre_actor_id
add index idx(actor_id);

# Identify original id of actors, if fake
drop table if exists eth_base.actor_alias;
create table eth_base.actor_alias
select actor_id
,if(original is null,actor_id,original) original
from eth_base.pre_actor_id id
left join eth_base.mergeAlias ma
on actor_id=fake;

alter table eth_base.actor_alias
add index idx (actor_id,original);

# List of actual actor ids involved in pull based software development
drop table if exists eth_base.actor_id;
create table eth_base.actor_id
select distinct original actor_id
from eth_base.actor_alias;

##########################
# Country of residence of actors
drop table eth_base.actor_country;
create table eth_base.actor_country
select actor_id
,country
from eth_base.actor_id
left join eth_base.country
on actor_id=user_id;

alter table eth_base.actor_country
add index actor(actor_id);

#############################
# Affiliation of actor
# Heuristics to identify the affiliation of the author

## Identify domain names used by company employees
drop table eth_base.actor_company_domain;
create table eth_base.actor_company_domain
select actor_id
,if(company is null or company='','None',company) company
,if(email is null or email='' or email not regexp '^[^@]+@[^@]+\.[^@]{2,}$','None',substring_index(substring_index(email,'@',-1),'.',1)) email_domain
from eth_base.actor_id
left join github_final.users 
on actor_id=id;

## Select companies which represent a significant count of contributors (excluding black list)
drop table eth_base.company;
create table eth_base.company
select company
,count(*) count
from github_final.users
where company is not null
and company not in ('','None','N/A','-','no','NULL','.','company','','No Company','nil','university','test','my own','Non','nope','Simple','Own','retired')
group by company
having count>=20;

## Identify all email domains for a company
create table eth_base.company_email_domain
select com.company
,substring_index(substring_index(usr.email,'@',-1),'.',1) email_domain
from eth_base.company com
left join github_final.users usr
on com.company=usr.company;

## Identify companies where email domain is same as the name of the company
## We use this as the criteria to identify the affiliation of developers who have mentioned email address and not the company 
drop table eth_base.map_company_email_domain;
create table eth_base.map_company_email_domain
select distinct company,email_domain
from eth_base.company_email_domain
where email_domain=company;

## Identify affiliation using company name and above-mentioned heuristics 
drop table eth_base.actor_affiliation;
create table eth_base.actor_affiliation
select actor_id
,if(com.company is null,if(map.company is null,'None',map.company),com.company) affiliation
from eth_base.actor_company_domain acd
left join eth_base.company com
on acd.company=com.company
left join eth_base.map_company_email_domain map
on acd.email_domain=map.email_domain;

## Indexing on actor id
alter table eth_base.actor_affiliation
add index actor(actor_id);

#############################
# Date of joining of actor
drop table if exists eth_base.actor_created_at;
create table eth_base.actor_created_at
select actor_id
,original
,created_at
from eth_base.actor_alias
left join github_final.users
on original=id;

alter table eth_base.actor_created_at
add index idx(actor_id,created_at);

######################################
# Actor Tenure
drop table if exists eth_base.actor_tenure;
create table eth_base.actor_tenure
select base_repo_id
,pull_request_id
,pa.actor_id
,original
,timestampdiff(month,ac.created_at,pa.created_at) actor_tenure
from eth_base.pr_actions pa
left join eth_base.actor_created_at ac
on pa.actor_id=ac.actor_id;

alter table eth_base.actor_tenure
add index idx(pull_request_id,original);
