# github_final is the name of the GHTorrent data made available in August 2015
# eth_base is the name of the database where the base tables required for analysis are created
# processed data is stored in eth database 

# Remove possibilities of sql injection in the data
## Update login 
update github_final.users
set login=replace(login,';','');

update github_final.users
set login=replace(login,'\n','');

update github_final.users
set login=replace(login,'"','');

update github_final.users
set login=replace(login,'\\','');

##################################
## Update name
update github_final.users
set name=replace(name,';','');

update github_final.users
set name=replace(name,'\n','');

update github_final.users
set name=replace(name,'"','');

update github_final.users
set name=replace(name,'\\','');

########################
# Update location
update github_final.users
set location=replace(location,';','');

update github_final.users
set location=replace(location,'\n','');

update github_final.users
set location=replace(location,'"','');

update github_final.users
set location=replace(location,'\\','');

#####################################
# Update email
update github_final.users
set email=replace(email,';','');

update github_final.users
set email=replace(email,'\n','');

update github_final.users
set email=replace(email,'"','');

update github_final.users
set email=replace(email,'\\','');