preprocess_submitters_survey_data <- function(file)
{
library(base)

submitters_raw_data=read.csv(file,header=T,sep=",",stringsAsFactors=FALSE)
attach(submitters_raw_data)
rows=nrow(submitters_raw_data)
columns=length(submitters_raw_data)
submitters<-data.frame(Response.ID,
                                    Age=Please.select.your.appropriate.age.level,
                                    Gender=rep(0,rows),
                                    Country,
                                    User.identified.country=Where.do.you.currently.reside.,
                                    Job=rep(0,rows),
                                    Role=rep(0,rows),
                                    Years.of.programming=How.many.years.have.you.been.programming.,
                                    Years.in.geographically.distributed.projects=How.many.years.have.you.worked.on.projects.that.are.developed.in.a.geographically.distributed.manner.,
                                    Years.in.oss=How.many.years.have.you.been.working.on.open.source.projects.,
                                    Importance.of.PR.Track.Record.On.PR.acceptance.decision=Pull.requester.track.record.Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision=Popularity.of.pull.requester.Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Importance.of.worked.together.in.past.on.PR.acceptance.decision=Worked.together.in.past.Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision=Project.membership.of.pull.requester.Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Importance.of.number.of.lines.changed.on.PR.acceptance.decision=Number.of.lines.changed.Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Importance.of.existence.of.test.on.PR.acceptance.decision=Existence.of.tests.in.the.pull.request.Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Importance.of.pull.requester.gender.on.PR.acceptance.decision=Pull.requester.gender..Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Importance.of.pull.requester.nationality.on.PR.acceptance.decision=Pull.requester.nationality.Please.rate.the.importance.that.the.following.factors.play.in.the.DECISION.TO.ACCEPT.OR.REJECT.a.pull.request.,
                                    Other.factors=What.social.factors..other.than.the.ones.mentioned.above..influence.the.chances.of.your.pull.requests.getting.accepted.,
                                    Bias=Did.you.experience.bias.based.on.your.nationality.in.getting.your.pull.requests.accepted..
)
for (i in 1:rows)
{
  # Gender
  if(Please.state.your.gender[i]=='Other - Write In')
  {
    submitters$Gender[i]=Other...Write.In.Please.state.your.gender[i]
  }
  else{
    submitters$Gender[i]=Please.state.your.gender[i] 
  }  
  # Job
  if(Who.do.you.work.for.[i]=='Other - Write In')
  {
    submitters$Job[i]=Other...Write.In.Who.do.you.work.for.[i]
  }
  else{
    submitters$Job[i]=Who.do.you.work.for.[i] 
  } 
  
  # Role
  if(Which.of.the.following.best.describes.your.role.in.GitHub.[i]=='Other - Write In')
  {
    submitters$Role[i]=Other...Write.In.Which.of.the.following.best.describes.your.role.in.GitHub.[i]
  }
  else{
    submitters$Role[i]=Which.of.the.following.best.describes.your.role.in.GitHub.[i] 
  }     
}


# Selected features in table
countries=table(submitters$Country)
countries=countries[order(countries,decreasing=T)]
submitters$Country_Mod=factor(submitters$Country,levels=names(countries[countries>=10]))

countries=table(submitters$User.identified.country)
countries=countries[order(countries,decreasing=T)]
submitters$User.identified.country_Mod=factor(submitters$User.identified.country,levels=names(countries[countries>=10]))

#table(submitters$Age)
submitters$Age_Mod=ordered(submitters$Age,levels=c("0 - 20 years","21- 30 years","31- 40 years","41 - 50 years",
                                                     "51 - 60 years","more than 60 years"))
#table(submitters$Gender)
submitters$Gender_Mod=factor(submitters$Gender,levels=c("Male","Female"))

job=table(submitters$Job)
job=job[order(job,decreasing=T)]
submitters$Job_Mod=ordered(submitters$Job,levels=names(job[job>=5 & names(job)!=""]))

for (i in 1:rows)
{
  if(submitters$Role[i]=="all of the above")
  {
    submitters$Role[i]="All of the above"
  }
}
role=table(submitters$Role)
role=role[order(role,decreasing=T)]
submitters$Role_Mod=factor(submitters$Role,levels=names(role[role>=5]))
####################################################

submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod=as.character(submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod=factor(submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod=
  as.character(submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod=factor(submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod=
  as.character(submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod=factor(submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod=
  as.character(submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod=factor(submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod=
  as.character(submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod=factor(submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod=
  as.character(submitters$Importance.of.existence.of.test.on.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod=factor(submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod=
  as.character(submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod=factor(submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod=
  as.character(submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision)
for(i in 1:rows)
{
  if(submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod[i]=="Very important" | submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod[i]=="Important")
  {
    submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod[i]="Very-Important"
  }
  if(submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod[i]=="Moderately important" | submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod[i]=="Of little importance")
  {
    submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod[i]="Moderate-Little"
  }
}
submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod=factor(submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_Mod,levels=c("Very-Important","Moderate-Little","Unimportant"))

submitters$Bias_Mod=factor(submitters$Bias,levels=c("Yes","No"))
####################################################
submitters$Gender=factor(submitters$Gender)
submitters$Years.of.programming=ordered(submitters$Years.of.programming,
                                        levels=c("less than 1 year","1 to 2 years","3 to 6 years","7 to 10 years","more than 10 years","Never"))
submitters$Years.in.geographically.distributed.projects=factor(submitters$Years.in.geographically.distributed.projects,
                                                               levels=c("less than 1 year","1 to 2 years","3 to 6 years","7 to 10 years","more than 10 years","Never"))
submitters$Years.in.oss=factor(submitters$Years.in.oss,
                               levels=c("less than 1 year","1 to 2 years","3 to 6 years","7 to 10 years","more than 10 years","Never"))

submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision=ordered(submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision,
                                                                           levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision=ordered(submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision,
                                                                                        levels=c("Very important","Important","Moderately important","Of little importance","Unimportant","I don't know"))
submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision=ordered(submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision,
                                                                                   levels=c("Very important","Important","Moderately important","Of little importance","Unimportant","I don't know"))
submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision=ordered(submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision,
                                                                                                levels=c("Very important","Important","Moderately important","Of little importance","Unimportant","I don't know"))
submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision=ordered(submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision,
                                                                                   levels=c("Very important","Important","Moderately important","Of little importance","Unimportant","I don't know"))
submitters$Importance.of.existence.of.test.on.PR.acceptance.decision=ordered(submitters$Importance.of.existence.of.test.on.PR.acceptance.decision,
                                                                             levels=c("Very important","Important","Moderately important","Of little importance","Unimportant","I don't know"))
submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision=ordered(submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision,
                                                                                 levels=c("Very important","Important","Moderately important","Of little importance","Unimportant","I don't know"))
submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision=ordered(submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision,
                                                                                      levels=c("Very important","Important","Moderately important","Of little importance","Unimportant","I don't know"))

submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision_1=ordered(submitters$Importance.of.PR.Track.Record.On.PR.acceptance.decision,
                                                                           levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision_1=ordered(submitters$Importance.of.popularity.of.pull.requester.on.PR.acceptance.decision,
                                                                                        levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision_1=ordered(submitters$Importance.of.worked.together.in.past.on.PR.acceptance.decision,
                                                                                   levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision_1=ordered(submitters$Importance.of.project.membership.of.pull.requester.on.PR.acceptance.decision,
                                                                                                levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision_1=ordered(submitters$Importance.of.number.of.lines.changed.on.PR.acceptance.decision,
                                                                                   levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.existence.of.test.on.PR.acceptance.decision_1=ordered(submitters$Importance.of.existence.of.test.on.PR.acceptance.decision,
                                                                             levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision_1=ordered(submitters$Importance.of.pull.requester.gender.on.PR.acceptance.decision,
                                                                                 levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision_1=ordered(submitters$Importance.of.pull.requester.nationality.on.PR.acceptance.decision,
                                                                                      levels=c("Very important","Important","Moderately important","Of little importance","Unimportant"))
return (submitters);
}
